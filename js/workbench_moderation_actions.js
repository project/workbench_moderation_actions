/* eslint-disable no-unused-vars */
/**
 * @file
 * Contains workbench_moderation_actions.js.
 */

(($, Drupal) => {
  /**
   * Ajax command for reloading the window.
   *
   * @param {Drupal.Ajax} ajax
   *   An Ajax object.
   * @param {object} response
   *   The Ajax response.
   * @param {string} response.selector
   *   The selector in question.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.reload = (ajax, response, status) => {
    window.location.reload();
  };
})(jQuery, Drupal);
