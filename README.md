# Workbench Moderation Actions

If you use Workbench Moderation, you'll soon find that Drupal core's 'Publish
content' and 'Unpublish content' actions don't work. This module replaces those
default actions with actions for each of your workbench moderation states,
enabling you to bulk set the workbench state of multiple nodes.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/workbench_moderation_actions).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/workbench_moderation_actions).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Workbench moderation](https://drupal.org/project/workbench_moderation)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will remove the 'Publish content' and 'Unpublish content'
actions and replace them with actions for each of your workbench moderation
states. You will see them in the "Actions" drop-down menu on the /admin/content
page.


## Maintainers

- Nic Rodgers - [nicrodgers](https://drupal.org/u/nicrodgers)

### Original development by:

- Daniel Wehner - [dawehner](https://drupal.org/u/dawehner)
